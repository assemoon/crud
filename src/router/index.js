import Vue from 'vue'
import VueRouter from 'vue-router'
import PostList from "@/components/PostList"
import showPost from "@/components/showPost";
import createPost from "@/components/createPost";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'PostList',
    component: PostList
  },
  {
    path: '/showPost/:id',
    name: 'showPost',
    component: showPost
  },
  {
    path: '/createPost',
    name: 'createPost',
    component: createPost
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
